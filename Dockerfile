FROM ghcr.io/puppeteer/puppeteer:21.1.1 as base

WORKDIR /app

USER root

ARG SCOPE
ENV SCOPE=$SCOPE

ARG PACKAGE_CONTAINERS="apps packages"
ARG CLEANING_TARGETS="src test .turbo .eslintrc.* jest.config.* tsup.config.* tsconfig.*"

ARG PORT=3000
ENV PORT=$PORT

RUN corepack enable
RUN npm install -g turbo

FROM base as installer
ADD . .

FROM base as builder
COPY --from=installer /app .
COPY --from=installer /app/out/pnpm-workspace.yaml .

FROM base as runner
COPY --from=builder /app .

RUN pnpm install -r --prod --ignore-scripts
RUN for c in $PACKAGE_CONTAINERS; do \
    for t in $CLEANING_TARGETS; do \
    rm -rf ./$c/*/$t; \
    done; \
    done;

EXPOSE $PORT

RUN chown -R pptruser:pptruser /app
USER pptruser

CMD pnpm run start
